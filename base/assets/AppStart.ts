// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;
import Utils from './Utils';

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    contentRoot: cc.Node = null;

    @property(cc.Node)
    exitButton: cc.Node = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {

        window.onresize = function () {
            Utils.resize();
        }.bind(this);

        Utils.resize();
        Utils.removeItemToPool(this.contentRoot);

        this.init();
    }

    urlParse() {
        var params = {};
        if (window.location == null) {
            return params;
        }
        var name, value;
        var str = window.location.href; //取得整个地址栏
        var num = str.indexOf("?")
        str = str.substr(num + 1); //取得所有参数   stringvar.substr(start [, length ]
    
        var arr = str.split("&"); //各个参数放到数组里
        for (var i = 0; i < arr.length; i++) {
            num = arr[i].indexOf("=");
            if (num > 0) {
                name = arr[i].substring(0, num);
                value = arr[i].substr(num + 1);
                params[name] = value;
            }
        }
        return params;
    }

    getLessonList(){
        var arr = [];
        arr.push({name:'05_技能CD效果',scene:'05_skill_cd'});
        arr.push({name:'06_背景地图拖拽',scene:'06_big_map'});
        arr.push({name:'07_方向与角度互换',scene:'07_rotation_to_dir'});
        arr.push({name:'08_横版RPG',scene:'08_rpg'});
        arr.push({name:'09_全屏幕适配',scene:'09_screen_fit'});
        arr.push({name:'10_3D麻将',scene:'10_3dmj'});

        return arr;
    }
    

    init () {
        var arr = this.getLessonList();
        var args = this.urlParse();
        if(!window.__dontCheckArgs && args.lesson){
            for(var i = 0; i < arr.length; ++i){
                var info = arr[i];
                if(info.name.indexOf(args.lesson) == 0){
                    this.loadScene(info.scene);
                    return;
                }
            }
        }


        for(var i = 0; i < arr.length; ++i){
            var item = Utils.addItemFromPool(this.contentRoot);
            var info = arr[i];
            item.getChildByName('name').getComponent(cc.Label).string = '' + (i+1) + '.' + info.name;
            item.getChildByName('scene').getComponent(cc.Label).string = info.scene + '.fire';
            item.__meta = info;
        }
    }

    onItemClicked(event){
        var info = event.target.__meta;
        this.loadScene(info.scene);
    }

    loadScene(scene){
        Utils.loadScene(scene,function(){ 
            cc.loader.loadRes('ExitButtonPrefab',function(err,prefab){
                var button = cc.instantiate(prefab);
                cc.find('Canvas').addChild(button);
            });
        });
    }

    onDontClick(event){
        cc.sys.openURL('https://qilinzi.blog.csdn.net/');
    }

    // update (dt) {}
}
