// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    body: cc.Node = null;

    @property(cc.Node)
    gun: cc.Node = null;

    @property(cc.Node)
    firePoint: cc.Node = null;

    @property(cc.Node)
    bullet: cc.Node = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        let self = this;
        this.schedule(function(){
            self.shoot();
        },0.2);
    }

    shoot(){
        let bullet = cc.instantiate(this.bullet);
        bullet.active = true;
        var root = cc.find('Canvas');
        root.addChild(bullet);
        var p = this.firePoint.convertToWorldSpaceAR(this.firePoint.position);
        var pos = root.convertToNodeSpaceAR(p);
        bullet.x = pos.x;
        bullet.y = pos.y;
        bullet.getComponent('07_bullet').setTarget(cc.find('Canvas/tank2'));
    }

    // update (dt) {}
}
